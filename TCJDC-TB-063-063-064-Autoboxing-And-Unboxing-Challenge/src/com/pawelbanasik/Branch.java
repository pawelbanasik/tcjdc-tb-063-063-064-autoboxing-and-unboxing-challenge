package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class Branch {

	private String name;
	private List<Customer> customers;

	public Branch(String name) {
		this.name = name;
		this.customers = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public boolean newCustomer(String customerName, double initialAmount) {
		if (findCustomer(customerName) == null){
		customers.add(new Customer(customerName, initialAmount));
		return true;
		} else {
			return false;
		}
	}

	public boolean addCustomerTransaction(String customerName, double amount) {
		if (findCustomer(customerName) != null) {
			findCustomer(customerName).addTransactions(amount);
			return true;
		} else {
			return false;
		}

	}

	// on to zrobil zwyklym for a ja foreach
	public Customer findCustomer(String customerName) {
		for (Customer customer : customers) {
			if (customer.getName().equals(customerName)) {
				return customer;
			}
		}
		return null;
	}

}
