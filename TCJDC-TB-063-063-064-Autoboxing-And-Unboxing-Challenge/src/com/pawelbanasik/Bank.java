package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class Bank {

	private String name;
	private List<Branch> branches;

	public Bank(String name) {
		this.name = name;
		this.branches = new ArrayList<>();
	}

	
	public Branch findBranch(String branchName) {
		for (Branch branch : branches) {
			if (branch.getName().equals(branchName)) {
				return branch;
			}
		}
		return null;
	}

	public boolean addBranch(String branchName) {
		if (findBranch(branchName) == null) {
			branches.add(new Branch(branchName));
			return true;
		} else {
			return false;
		}

	}

	public boolean addCustomer(String branchName, String customerName, double initialAmount) {
		if (findBranch(branchName) != null) {
			findBranch(branchName).newCustomer(customerName, initialAmount);
			return true;
			
		} else {
			return false;
		}
	}

	public boolean addCustomerTransaction(String branchName, String customerName, double amount) {
		if (findBranch(branchName) != null) {
			findBranch(branchName).addCustomerTransaction(customerName, amount);
			return true;
			
		} else {
			return false;
		}
	}
	 public boolean listCustomers(String branchName, boolean showTransactions) {
	       Branch branch = findBranch(branchName);
	       if(branch != null) {
	           System.out.println("Customer details for branch " + branch.getName());

	           List<Customer> branchCustomers = branch.getCustomers();
	           for(int i=0; i<branchCustomers.size(); i++) {
	               Customer branchCustomer = branchCustomers.get(i);
	               System.out.println("Customer: " + branchCustomer.getName() + "[" + (i+1) + "]");
	               if(showTransactions) {
	                   System.out.println("Transactions");
	                   List<Double> transactions = branchCustomer.getTransactions();
	                   for(int j=0; j<transactions.size(); j++) {
	                       System.out.println("[" + (j+1) + "]  Amount "  + transactions.get(j));
	                   }
	               }
	           }
	           return true;
	       } else {
	           return false;
	       }

	}

}
