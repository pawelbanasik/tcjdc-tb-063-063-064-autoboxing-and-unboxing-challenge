package com.pawelbanasik;

import java.util.ArrayList;
import java.util.List;

public class Customer {

	private String name;
	private List<Double> transactions;

	// w konstruktorze mialem jako parametr liste on ma double
	public Customer(String name, double inialAmount) {
		this.name = name;
		this.transactions = new ArrayList<>();
		// nie mialem tego ponizej bo dodawalem to w branchu
		addTransactions(inialAmount);
	}

	public String getName() {
		return name;
	}

	// to mialem inaczej - nie mialem tej metody
	public void addTransactions(double amount) {
		transactions.add(amount);
	}

	public List<Double> getTransactions() {
		return transactions;
	}
	
	
	
}
